import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import tensorflow as tf 
from tensorflow import keras
from shutil import copy2
import os
from tensorflow.keras.preprocessing import image_dataset_from_directory


testData = pd.read_csv('Test.csv', index_col = 0)
print(testData)
path = os.getcwd()
os.makedirs('Test_Dir', exist_ok=True)
for i in range (43):
    os.makedirs(path + '/Test_Dir/' + str(i), exist_ok=True)
    
    
for _, row in testData.iterrows():
    endpath = row.Path.split('/')
    name = endpath[1]
    copy2(row.Path, path + '/Test_Dir/' + str(row.ClassId) + '/' + name)


datasetTest = keras.utils.image_dataset_from_directory('Test_Dir')
datasetTrain = keras.utils.image_dataset_from_directory('Train')