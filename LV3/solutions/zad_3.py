import urllib
import pandas as pd
import xml.etree.ElementTree as ET
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=01.1.2017&vrijemeDo=01.12.2017'
aqHR = urllib.request.urlopen(url).read()
root = ET.fromstring(aQHR)
df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme, utc=True)
df.plot(y='mjerenje', x='vrijeme');
#1
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek
#2
sortedData = df.sort_values("mjerenje")
print('Tri dana s najvecim koncentracijama P10 cestica: \n',sortedData[['mjerenje','vrijeme','dayOfweek','month']].tail(3))
#3
missingDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31] - df.groupby("month").mjerenje.count()
missingDays.plot.bar()
#4
comparison = df[(df.month == 11) | (df.month == 7)]
comparison.boxplot(column = ['mjerenje'], by = 'month')
#5
df['Vikend'] = ((df.dayOfweek >= 5) & (df.dayOfweek <= 6))
df.boxplot(column = ['mjerenje'], by='Vikend')
df['dayOfweek'] = ((df.dayOfweek >= 0) & (df.dayOfweek <= 4))
df.boxplot(column = ['mjerenje'], by='dayOfweek')
