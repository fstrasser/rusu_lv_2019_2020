import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

cars = pd.read_csv('../resources/mtcars.csv')
# print(len(cars))
# print(cars)
# 1
# prvi = cars.groupby('cyl').mean()
# print(prvi)
# plt.bar([4, 6, 8], prvi['mpg'], align='center', alpha= 0.5)
# plt.show()
# 2
# drugi = cars[['cyl', 'wt']]
# drugi.boxplot(by='cyl')
# plt.show()
# 3
# treci = cars.groupby('am').mean()
# print(treci)
# plt.bar([0, 1], treci['mpg'], align='center', alpha=1, linewidth = 0.1)
# plt.show()
# 4
# plt.scatter(cars[cars.am == 0]['qsec'], cars[mtcars.am == 0]['hp'], label='automatic', s=cars[cars.am == 0]['wt']*20)
# # plt.scatter(cars[cars.am == 1]['qsec'], cars[mtcars.am == 1]['hp'], label='manual', s=cars[cars.am == 1]['wt']*20)
# # plt.legend(loc=1)
# # plt.show()

print(cars.tail(4))