import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')
# print(len(mtcars))
# print(mtcars)

mtcars_potrosnja = mtcars.sort_values('mpg')
# print(mtcars_potrosnja)
# print(mtcars_potrosnja.tail(5)['car'])

mtcars_8cyl_minpotr = mtcars[mtcars.cyl == 8].sort_values('mpg')
# print(mtcars_8cyl_minpotr.head(3)['car'])

mtcars_6cyl = mtcars[mtcars.cyl == 6]
# print(mtcars_6cyl)
mtcars_6cyl_mean = mtcars_6cyl['mpg'].mean()
# print(mtcars_6cyl_mean)

mtcars4 = mtcars[(mtcars.cyl == 4) & (mtcars.wt>=2.000) & (mtcars.wt<=2.200) ]
mtcars41 = mtcars['wt']
# print(mtcars4)
mtcars4_mpg = mtcars4['mpg']
# print(mtcars4_mpg.mean())

mtcars5_len = len(mtcars)
mtcars5_automatic = mtcars[mtcars.am == 0]
mtcars5_automatic_len = len(mtcars5_automatic)
# print("Automatic num: ", mtcars5_automatic_len, " Manual: ", mtcars5_len - mtcars5_automatic_len)

mtcars6 = mtcars[(mtcars.am == 0) & (mtcars.hp > 100)]
# print(len(mtcars6))

coeff_kg = 0.45359237
mtcars7 = mtcars[['car', 'wt']]
mtcars7.wt *= coeff_kg*1000
print(mtcars7)