import numpy as np
import matplotlib.pyplot as plt
import sklearn.datasets as datasets
import sklearn.linear_model as lm
import random


def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data

np.random.seed(242)
dataset = generate_data(200)
np.random.seed(12)
data_train = generate_data(100)

colors = ["b" if data>=0.5 else "r" for data in dataset[:,2]]

plt.scatter(
   dataset[:, 0], dataset[:, 1],
   c= colors , marker='.',
)

lm = lm.LogisticRegression()
lm = lm.fit(dataset[:,:2], dataset[:,2])
predict = lm.predict(dataset[:,:2])

x2 = (-(lm.intercept_ + lm.coef_[0,0]*-4))/lm.coef_[0,1]
x22 = (-(lm.intercept_ + lm.coef_[0,0]*4))/lm.coef_[0,1]

plt.plot([-4,4],[x2,x22])



