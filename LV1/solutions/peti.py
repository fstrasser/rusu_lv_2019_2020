fname = input("Enter file name: ")
fh = open(fname)
count=0
total=0
string="X-DSPAM-Confidence:"
for line in fh:
    if not line.startswith("X-DSPAM-Confidence:"):
        continue
    else:
        count=count+1
        num=float(line[len(string)+1:])
        total=total+num

print("Average X-DSPAM-Confidence:",float(total/count))