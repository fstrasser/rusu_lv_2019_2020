
try:
    num=float(input("Unesite broj od 0.0 do 1.0: "))
except Exception:
    print("Nije broj!")
    exit()

if(num>=0.9 and num<=1): 
    print("A")
elif(num>=0.8 and num<0.9): 
    print("B")
elif(num>=0.7 and num<0.8): 
    print("C")
elif(num>=0.6 and num<0.7): 
    print("D")
elif(num>=0.0 and num<0.6): 
    print("F")
else:
    print("Uneseni broj nije unutar zadanog intervala!")