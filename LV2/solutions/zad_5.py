import numpy as np
import matplotlib.pyplot as plt
import pandas as p

potrosnje=[]
figure=plt.figure() 
plot=figure.add_subplot(1,1,1)
auti=p.read_csv("../resources/mtcars.csv")
plt.scatter(auti.mpg,auti.hp)
plt.title("Ovisnost potrosnje o konjskim snagama")
plt.xlabel("Potrosnja")
plt.ylabel("Konjske snage")
plt.grid(linestyle="--")
for i in range(0,len(auti.mpg)):
    plot.annotate("%.2f"%auti.wt[i],xy=(auti.mpg[i],auti.hp[i]),xytext=(auti.mpg[i],auti.hp[i]),arrowprops=dict(arrowstyle="-",facecolor="cyan"))
    potrosnje.append(auti.mpg[i])

plt.show()

srvr=np.average(potrosnje)
mini=min(potrosnje)
maksi=max(potrosnje)

print("Srednja vrijednost:%.3f"%srvr," maksimalna:%.2f"%maksi," minimalna:%.2f"%mini)



