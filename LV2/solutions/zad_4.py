import numpy as np
import matplotlib.pyplot as plt

np.random.seed(0) 
dice = np.random.randint(low=1, high=7, size=100)
print(dice)
plt.hist(dice, bins=20)