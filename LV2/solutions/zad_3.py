import numpy as np
import matplotlib.pyplot as plt

np.random.seed(100) # postavi seed generatora brojeva
randNum = np.random.randint(2, size=1000)
male = []
female = []
for i in randNum:
    if i == 0:
        female.append(np.random.normal(167, 7))
    else:
        male.append(np.random.normal(180, 7))
plt.hist([male, female], color = ['blue', 'red'])
avgMale = np.average(male)
avgFemale = np.average(female)
plt.axvline(avgMale, color='purple')
plt.axvline(avgFemale, color='black')
plt.legend(["m", "z", "avgMale", "avgFemale"])