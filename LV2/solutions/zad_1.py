import re

file = open('../resources/mbox-short.txt')
fhand = file.read()


mails = re.findall('[a-zA-Z0-9_.]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+', fhand)

names = []

for mail in mails:
    names.append(mail[:mail.find('@')])
    
print(names)
print("Len of names is ", len(names))
