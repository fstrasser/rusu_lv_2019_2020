from sklearn import datasets
import pandas as pd
from matplotlib import pyplot as plt
import numpy as np


np.random.seed(123)


def euclidean_distance(x1,x2):
    return np.sqrt(np.sum(x1-x2)**2)


def closest_centroid(sample,centroids):
    distances = [euclidean_distance(sample, point) for point in centroids]
    closest_idx = np.argmin(distances)
    return closest_idx
    

def create_clusters(X,K,centroids):
    clusters = [[] for _ in range(K)]
    for idx, sample in enumerate(X):
        centroid_idx = closest_centroid(sample, centroids)
        clusters[centroid_idx].append(idx)
        return clusters
    
def get_centroids(X,K,n_features,clusters):
    centroids = np.zeros((K,n_features))
    for cluster_idx, cluster in enumerate(clusters):
        cluster_mean = np.mean(X[cluster],axis=0)
        centroids[cluster_idx] = cluster_mean
    return centroids

def is_converged(K,centroids_old,centroids):
    distances = [euclidean_distance(centroids_old[i], centroids[i]) for i in range(K)]
    return sum(distances) == 0    

def get_cluster_labels(clusters,n_samples):
    labels = np.empty(n_samples)
    for cluster_idx, cluster in enumerate(clusters):
        for sample_idx in cluster:
            labels[sample_idx] = cluster_idx  
    return labels
     

def plot(X,clusters,centroids):       
    fig, ax = plt.subplots(figsize=(12,8))
    
    for i, index in enumerate(clusters):
        point = X[index].T
        ax.scatter(*point)
        
    for point in centroids:
        ax.scatter(*point, marker="x",color="black",linewidth=2)
        
    plt.show()
    
def kMeans(X,K,max_iters):
    
    clusters = [[] for _ in range(K)]
    centroids = []
    
  
    n_samples, n_features = X.shape
    random_sample_idxs = np.random.choice(n_samples,K,replace=False)
    centroids = [X[idx] for idx in random_sample_idxs]
     
    for _ in range(max_iters):
        
        clusters = create_clusters(X, K, centroids)
        
        centroids_old = centroids
        centroids = get_centroids(X,K,n_features,clusters)
        
        if is_converged(K,centroids_old,centroids):
            break
        
        
    
    plot(X,clusters,centroids)
    return get_cluster_labels(clusters,n_samples)

def generate_data(n_samples, flagc):

 if flagc == 1:
     random_state = 365
     X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)

 elif flagc == 2:
     random_state = 148
     X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
     transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
     X = np.dot(X, transformation)

 elif flagc == 3:
     random_state = 148
     X, y = datasets.make_blobs(n_samples=n_samples,
                       cluster_std=[1.0, 2.5, 0.5, 3.0],
                       random_state=random_state)
 elif flagc == 4:
     X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)

 elif flagc == 5:
     X, y = datasets.make_moons(n_samples=n_samples, noise=.05)

 else:
     X = []

 return X 

generated_data = generate_data(500,1)
#generated_data = generate_data(500,2)
#generated_data = generate_data(500,3)
#generated_data = generate_data(500,4)
#generated_data = generate_data(500,5)




df = pd.DataFrame(generated_data,columns=['x','y'])
plt.scatter(df.x, df.y)

plt.scatter(df.x,df.y,color='red')  


predicted_cluster = kMeans(generated_data,K=3,max_iters=100)
df['cluster']= predicted_cluster


# df1=df[df.cluster==0]
# df2=df[df.cluster==1]
# df3=df[df.cluster==2]


# plt.scatter(df1.x,df1.y,color='red')
# plt.scatter(df2.x,df2.y,color='green')
# plt.scatter(df3.x,df3.y,color='blue')


