from sklearn import datasets
import pandas as pd
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import numpy as np
from scipy.cluster.hierarchy import dendrogram, linkage 


def generate_data(n_samples, flagc):

 if flagc == 1:
  random_state = 365
  X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)

 elif flagc == 2:
  random_state = 148
  X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
  transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
  X = np.dot(X, transformation)

 elif flagc == 3:
  random_state = 148
  X, y = datasets.make_blobs(n_samples=n_samples,
  cluster_std=[1.0, 2.5, 0.5, 3.0],
  random_state=random_state)
 elif flagc == 4:
  X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)

 elif flagc == 5:
  X, y = datasets.make_moons(n_samples=n_samples, noise=.05)

 else:
  X = []

 return X

#Zadatak 1.


generated_data = generate_data(500,1)
#generated_data = generate_data(500,2)
#generated_data = generate_data(500,3)
#generated_data = generate_data(500,4)
#generated_data = generate_data(500,5)




df = pd.DataFrame(generated_data,columns=['x','y'])
plt.scatter(df.x, df.y)

kmeans_model = KMeans(n_clusters=3)
predicted_cluster = kmeans_model.fit_predict(df[['x','y']])
df['cluster']= predicted_cluster
centroids=kmeans_model.cluster_centers_

df1=df[df.cluster==0]
df2=df[df.cluster==1]
df3=df[df.cluster==2]


plt.scatter(df1.x,df1.y,color='red')
plt.scatter(df2.x,df2.y,color='green')
plt.scatter(df3.x,df3.y,color='blue')
plt.scatter(centroids[:,0],centroids[:,1],marker='*',color='black',)

#Zadatak 2.

vrijednosti_krit_funkcije = []
for i in range(2,20):
    kmeans_model = KMeans(n_clusters=i)
    kmeans_model.fit(df[['x','y']])
    vrijednosti_krit_funkcije.append(kmeans_model.inertia_)
   

plt.figure()    
plt.xlabel('broj klastera')
plt.ylabel('vrijednost kriterijske funkcije')
plt.xticks(range(1,20))
plt.plot(range(2,20),vrijednosti_krit_funkcije)   

#Optimalan broj klastera može se odredit pomocu "Elbow metode":
#Na dobivenom grafu tražimo broj klastera za koji priakzani podaci prave "lakat"
#U ovom slucaju to su tri klastera


#Zadatak 3.

plt.figure()
dend = dendrogram(linkage(generated_data,method="ward"))











